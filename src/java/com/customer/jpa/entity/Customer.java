/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.customer.jpa.entity;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mdayacap
 */
@XmlRootElement
public class Customer {

    private String firstName;
    private String middleName;
    private String lastName;

    public Customer(){
    }
    
    public Customer(String firstName, String middleName, String lastName) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
