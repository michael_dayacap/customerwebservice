/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.customer.restful.resource;

import com.customer.jpa.entity.Customer;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;

/**
 * REST Web Service
 *
 * @author mdayacap
 */
@Path("/customer")
@RequestScoped
public class CustomerResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of CustomerResource
     */
    public CustomerResource() {
    }

    @GET
    @Produces("application/json")
    @Path("list")
    public List<Customer> list() {
        List<Customer> customers = new ArrayList<Customer>();
        customers.add(new Customer("Michael", "T.", "Dayacap"));
        customers.add(new Customer("Juan", "S.", "Dela Cruz"));
        customers.add(new Customer("John", "F.", "Doe"));
        return customers;
    }

    @POST
    @Consumes("application/json")
    @Path("create")
    public void create(Customer customer) {
        System.out.println("DEBUG TEST - create request -\n\tCustomer:\n\t\tfirstName=" + customer.getFirstName() + "\n\t\tmiddleName=" + customer.getMiddleName() + "\n\t\tlastName=" + customer.getLastName());
    }
}
