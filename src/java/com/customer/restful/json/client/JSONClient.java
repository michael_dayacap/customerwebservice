/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.customer.restful.json.client;

import com.customer.jpa.entity.Customer;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.json.JSONJAXBContext;
import com.sun.jersey.api.json.JSONMarshaller;
import java.io.StringWriter;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;

/**
 *
 * @author mdayacap
 */
public class JSONClient {

    public static void main(String[] args) throws JAXBException {
        Client client = Client.create();
        WebResource resource = client.resource("http://localhost:8080/CustomerWebService/customer");

        // Get response from as String
        String respStr = resource.path("list").accept(MediaType.APPLICATION_JSON).get(String.class);
        System.out.println(respStr);

        // Get response from as String
        Customer customer = new Customer("Michael", "T.", "Dayacap");
        JSONJAXBContext ctx = new JSONJAXBContext(Customer.class);
        JSONMarshaller jsonM = ctx.createJSONMarshaller();
        StringWriter writer = new StringWriter();
        jsonM.marshallToJSON(customer, writer);
        System.out.println(writer);
        resource.path("create").accept(MediaType.APPLICATION_JSON).entity(writer.toString(), MediaType.APPLICATION_JSON).post();
    

    }
}
